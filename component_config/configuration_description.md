All 4 modes share the same configuration parameter `mode`, which must have a value of one of the 4 modes mentioned above, namely:

- `sync_tables`
- `fetch_new_tables`
- `historize_tables`
- `remove_obsolete_tables`

Each of the 4 modes then take their own set of parameters, which will be described in more detail below. Alternatively, some modes share connection settings to Snowflake and MySQL instances to operate.

### Shared Parameters

`mysql` and `snowflake` are the only two parameters (besides `mode` and mode `parameters`), which are shared between different modes. In all modes, the configuration of these parameters looks the same.

For MySQL connection setup, you need to include the host, port, username and password (encrypted), which will be used to connect. The sample configuration then looks as follows:

```json
{
  "mysql": {
    "host": "<HOST>",
    "port": "<PORT>",
    "username": "<USER>",
    "#password": "<PASS>"
  }
}
```

For Snowflake connection, account, warehouse, username and password need to be included. The account can be [obtained from the URL of your instance](https://docs.snowflake.com/en/user-guide/connecting.html#your-snowflake-account-identifier); [the warehouse](https://docs.snowflake.com/en/user-guide/warehouses.html) needs to be created directly in the instance. Optionally, the role parameter can be provided as well, to use specified role; if not provided, default role will be used. The sample configuration then looks as follows:

```json
{
  "snowflake": {
    "account": "<ACCOUNT_IDENTIFIER>",
    "warehouse": "<WAREHOUSE>",
    "username": "<USER>",
    "#password": "<PASS>",
    "role": "<ROLE"
  }
}
```

All parameters setting up the modes, are grouped under `parameters` attribute.

### `sync_tables` mode

The `sync_tables` mode syncs the tables and columns from the source bucket into the destination bucket. This mode is only useful, if you're using `append_mode` in your MySQL BINLOG extractor. If the table does not exist in the destination bucket, it will be created as a view from the source bucket, containing only the non-deleted rows and latest changes. If a column is missing/remaining the destination bucket over the source bucket, the column will be added/dropped and view regenerated.

The `sync_tables` mode sample configuration looks like this:

```json
{
  "mode": "sync_tables",
  "mysql": {
    ...
  },
  "snowflake": {
    ...
  },
  "parameters": {
    "schemas": [
      "cdc"
    ],
    "exclude_tables": [
      "test_table"
    ],
    "#sapi_token": "<SAPI_TOKEN>",
    "source_bucket": "in.c-cdc-keboola-test",
    "destination_bucket": "in.c-cdc-keboola-test-view"
  }
}
```

The `mode`, `mysql` and `snowflake` parameters were described above. Mode related parameters are stored under `parameters`. Allowed parameters are:

- `schemas` - a schema, from which the tables are synced into KBC;
- `exclude_tables` - a list of tables in storage to exclude from syncing between source and destination bucket;
- `#sapi_token` - the Keboola Storage API token, which has full access to both `source_bucket` and `destination_bucket`;
- `source_bucket` - a bucket containing tables with raw data (`append_mode=True`);
- `destination_bucket` - a bucket, where views with latest data will be created.

### `fetch_new_tables` mode

The `fetch_new_tables` mode connects to the MySQL instance, detects tables, which are not in the replication and automatically adds them to the replication.

The sample configuration is:

```json
{
  "mode": "fetch_new_tables",
  "mysql": {
    ...
  },
  "parameters": {
    "schemas": [
      "cdc"
    ],
    "exclude_tables": [
      "cdc.test_table"
    ],
    "add_to_orchestration": true,
    "add_to_orchestration_id": "722301664",
    "add_to_orchestration_phase": "Replicate",
    "tables_in_configuration": 3,
    "index_configurations": true,
    "side_backfill": {
      "use": true,
      "rows": 5000,
      "columns": 3
    },
    "#sapi_token": "<SAPI_TOKEN>",
    "cdc_configuration": {
      "appendMode": true,
      "outputBucket": "cdc-keboola-test",
      "sshTunnel": false,
      "verifyCert": true,
      "ssl": false,
      "storageMappingsFile": "mappings",
      "fetchObjectsOnly": false,
      "runIncrementalSync": true,
      "databases": [
        "cdc"
      ]
    }
  }
}
```

The `mode` and `mysql` parameters were described above. Mode related parameters are stored under `parameters`. Allowed parameters are:

- `schemas` - a schema, from which the tables are synced into KBC;
- `exclude_tables` - a list of tables in MySQK to exclude from being added to the replication, must be specified in full_form (`<schema>.<table>`);
- `#sapi_token` - the Keboola Storage API token, which needs to have access to CDC configurations;
- `add_to_orchestration` - a boolean whether to add new configurations with tables to sync orchestration;
- `add_to_orchestration_id` - if `add_to_orchestration` is `true`, specify the id of the orchestration, to which new tasks will be added;
- `add_to_orchestration_phase` - specify a phase in the orchestration, to which new tasks will be added;
- `tables_in_configuration` - limits the number of tables, which can be in a single configuration, before a new one is created, allows for grouping of multiple tables into a single configuration;
- `index_configurations` - a boolean marking, whether new configurations should be prefixed with numeric index;
- `side_backfill.use` - whether to use a side backfill. If all conditions are met (row or size or both), new tables exceeding the specified limits will be backfilled in a separate configuration, before being added to the production orchestration, in order to not slow down the process;
- `side_backfill.rows` - specifies the number of rows above which a table is considered for separate syncing on the side;
- `side_backfill.size` - specifies the size of the table in MB above which a table is considered for separate syncing on the side;
- `cdc_configuration` - a valid JSON configuration of the `kds-team.ex-mysql-next` component, which will be used to create new configurations.

### `historize_tables` mode

The `historize_tables` mode connects to the Snowflake instance, detects changes, which are no longer needed in view generation and saves aside these changes to speed up view execution from `sync_tables` mode. This mode is only useful, if you're using `append_mode=True` mode.

The sample configuration is this:

```json
{
  "mode": "historize_tables",
  "snowflake": {
    ...
  },
  "parameters": {
    "query": "SELECT 'ACCOUNT' AS table_name;",
    "schema_raw": "<SCHEMA_RAW>",
    "schema_view": "<SCHEMA_VIEW>",
    "schema_history": "<SCHEMA_HISTORY>",
    "schema_backup": "<SCHEMA_BACKUP>",
    "schema_inactive": "<SCHEMA_INACTIVE>",
    "schema_tmp": "<SCHEMA_TMP>",
    "#sapi_token": "<SAPI_TOKEN>",
    "cdc_orchestration_id": "734363761",
    "historize_in_storage": true
  }
}
```

The `mode` and `snowflake` parameters were described above. Mode related parameters are stored under `parameters`. Allowed parameters are:

- `query` - A query, which will be executed in the Snowflake instance to get tables to historize. All tables to historize must be in returned in column `TABLE_NAME`;
- `schema_raw` - a schema in Snowflake, which contains the raw data (`append_mode=True`);
- `schema_view` - a schema in Snowflake, which contains the data with views;
- `schema_history` - a schema in Snowflake, which should contain historized data;
- `schema_backup` - a schema in Snowflake, which will contain backed up data, in case of application failure;
- `schema_inactive` - a schema in Snowflake, which will be used to generate views, with inactive data;
- `schema_tmp` - a schema in Snowflake, which will be used for temporary data storage. All tables are removed from this schema after successful run;
- `#sapi_token` - the Keboola Storage API token, which needs to have access to CDC configurations and orchestration;
- `cdc_orchestration_id` - the id of the orchestration, which is used for syncing the data. This orchestration will be stopped during historization process and re-enabled after finish;
- `historize_in_storage` - create tables with historical data in storage as well. All tables will be created as a bucket with name of `schema_history`.

### `remove_obsolete_tables` mode

The `remove_obsolete_tables` mode connects to the MySQL instance, detects active tables and removes from the replication all the tables, which can no longer be replicated and thus frees up spaces in the replication.

The sample configuration is this:

```json
{
  "mode": "remove_obsolete_tables",
  "mysql": {
    ...
  },
  "parameters": {
    "schemas": [
      "cdc"
    ],
    "exclude_tables": [
      "cdc.customers_60M"
    ],
    "#sapi_token": "<SAPI_TOKEN>"
  }
}
```

The `mode` and `mysql` parameters were described above. Mode related parameters are stored under `parameters`. Allowed parameters are:

- `schemas` - a schema, from which to read current_tables;
- `exclude_tables` - tables, which to keep in the replication, even though they no longer exist in the MySQL instance;
- `#sapi_token` - the Keboola Storage API token, which needs to have access to CDC configurations.