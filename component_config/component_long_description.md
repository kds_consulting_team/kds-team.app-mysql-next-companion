# MySQL BINLOG Companion App

This application serves as a companion app to the `kds-team.ex-mysql-next`, an extractor that is able to replicate the data from binary logs and include:

- inserted rows
- updated rows
- deleted rows

The application serves as an automation tool for a fully automated CDC sync.