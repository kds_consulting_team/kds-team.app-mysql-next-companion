from component import Component
from keboola.component.base import UserException

import logging

if __name__ == '__main__':
    c = Component()
    try:
        c.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(1)
