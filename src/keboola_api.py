import json
import requests
import time
from urllib.parse import urljoin

from keboola.http_client import HttpClient
from typing import Union


REGION_SPECIFIERS = {
    'AWS_us-east-1': '',
    'AWS_eu-central-1': 'eu-central-1',
    'AZ_north-europe': 'north-europe.azure'
}

API_PREFIXES = {
    'storage': 'connection',
    'encryption': 'encryption',
    'syrup': 'syrup'
}


def build_api_url(api: str, region: str, url: str = None):

    api_prefix = API_PREFIXES.get(api)
    region_infix = REGION_SPECIFIERS.get(region)

    if api_prefix is None:
        raise ValueError(f'Unsupported API prefix {api} provided; must be one of {list(API_PREFIXES.keys())}.')

    elif url is not None:
        return f'https://{api_prefix}.{url}'

    elif region_infix is None:
        raise ValueError(f"Region specifier {region_infix} not supported "
                         f"must be one of {list(REGION_SPECIFIERS.keys())}.")

    else:
        if region_infix == '':
            return f'https://{api_prefix}.keboola.com/'

        else:
            return f'https://{api_prefix}.{region_infix}.keboola.com/'


class StorageApi(HttpClient):

    API_IDENTIFIER = 'storage'
    API_SUFFIX = '/v2/storage/'

    def __init__(self, region_specifier: str, sapi_token: str, url: str = None) -> None:

        _base_url = urljoin(build_api_url(self.API_IDENTIFIER, region_specifier, url), self.API_SUFFIX)
        _base_hdr = {'x-storageapi-token': sapi_token}

        super().__init__(base_url=_base_url, auth_header=_base_hdr)

    def get_tables_in_bucket(self, bucket: str, include: str = None) -> requests.Response:

        return self.get_raw(f"buckets/{bucket}/tables", params={'include': include})

    def drop_table(self, table: str, force: bool = True) -> requests.Response:

        par_table = {'force': force}
        return self.delete_raw(f"tables/{table}", params=par_table)

    def truncate_table(self, table: str) -> requests.Response:

        return self.delete_raw(f"tables/{table}/rows")

    def get_all_configurations(self, component_id: str) -> requests.Response:

        return self.get_raw(f'components/{component_id}/configs')

    def get_configuration_detail(self, component_id: str, configuration_id: str) -> requests.Response:

        return self.get_raw(f'components/{component_id}/configs/{configuration_id}')

    def create_new_configuration(self, component_id: str, config_json: dict) -> requests.Response:

        configs_hdr = {'content-type': 'application/x-www-form-urlencoded'}
        return self.post_raw(f'components/{component_id}/configs', headers=configs_hdr, data=config_json)

    def update_configuration(self, component_id: str, config_id: str, config_json: dict) -> requests.Response:

        configs_hdr = {'content-type': 'application/x-www-form-urlencoded'}
        return self.put_raw(f'components/{component_id}/configs/{config_id}', headers=configs_hdr, data=config_json)

    def delete_configuration(self, component_id: str, configuration_id: str) -> requests.Response:

        return self.delete_raw(f'components/{component_id}/configs/{configuration_id}')

    def create_new_configuration_row(self, component_id: str, configuration_id: str,
                                     row_config_json: dict) -> requests.Response:

        rows_hdr = {'content-type': 'application/x-www-form-urlencoded'}
        return self.post_raw(f'components/{component_id}/configs/{configuration_id}/rows',
                             headers=rows_hdr, data=row_config_json)

    def update_configuration_row(self, component_id: str, configuration_id: str, row_id: str,
                                 row_config_json: dict) -> requests.Response:

        rows_hdr = {'content-type': 'application/x-www-form-urlencoded'}
        return self.put_raw(f'components/{component_id}/configs/{configuration_id}/rows/{row_id}',
                            headers=rows_hdr, data=row_config_json)

    def create_table_snaphot(self, table_id: str, description: str = None) -> requests.Response:

        snapshot_hdr = {'Content-Type': 'application/x-www-form-urlencoded'}
        snapshot_body = {'description': description}

        return self.post_raw(f'tables/{table_id}/snapshots', headers=snapshot_hdr, data=snapshot_body)

    def create_table_metadata(self, table_id: str, metadata_provider: str,
                              metadata_list: list) -> requests.Response:

        metadata_hdr = {'Content-Type': 'application/x-www-form-urlencoded'}
        metadata_dict = {}

        for idx in range(len(metadata_list)):
            try:
                metadata_dict[f'metadata[{idx}][key]'] = metadata_list[idx]['key']
                metadata_dict[f'metadata[{idx}][value]'] = metadata_list[idx]['value']
            except KeyError as e:
                KeyError(f"Key {e} missing at index {idx}.")

        metadata_body = {**{'provider': metadata_provider}, **metadata_dict}

        return self.post_raw(f'tables/{table_id}/metadata', headers=metadata_hdr, data=metadata_body)

    def create_column_metadata(self, column_id: str, metadata_provider: str, metadata_list: list) -> requests.Response:

        metadata_hdr = {'Content-Type': 'application/x-www-form-urlencoded'}

        metadata_dict = dict()

        for idx in range(len(metadata_list)):
            try:
                metadata_dict[f'metadata[{idx}][key]'] = metadata_list[idx]['key']
                metadata_dict[f'metadata[{idx}][value]'] = metadata_list[idx]['value']
            except KeyError as e:
                KeyError(f"Key {e} missing at index {idx}.")

        metadata_body = {**{'provider': metadata_provider}, **metadata_dict}

        return self.post_raw(f'columns/{column_id}/metadata', headers=metadata_hdr, data=metadata_body)

    def remove_primary_key(self, table_id: str) -> requests.Response:
        return self.delete_raw(f'tables/{table_id}/primary-key/')

    def create_column(self, table_id: str, column_name: str) -> requests.Response:

        column_data = {'name': column_name}
        return self.post_raw(f'tables/{table_id}/columns', data=column_data)

    def drop_column(self, table_id: str, column_name: str, force: bool = True) -> requests.Response:

        parameters = {'force': force}
        return self.delete_raw(f'tables/{table_id}/columns/{column_name}', params=parameters)

    def get_column_metadata(self, column_id: str) -> requests.Response:

        return self.get_raw(f'columns/{column_id}/metadata')


class EncryptionApi(HttpClient):

    API_IDENTIFIER = 'encryption'

    def __init__(self, region_specifier: str, component_id: str, project_id: str = None,
                 config_id: str = None, url: str = None) -> None:

        _base_url = build_api_url(self.API_IDENTIFIER, region_specifier, url)
        _default_par = {
            'componentId': component_id,
            'projectId': project_id,
            'configId': config_id
        }

        super().__init__(base_url=_base_url, default_params=_default_par)

    def encrypt_data(self, data: Union[str, dict], is_json: bool = True) -> Union[str, dict]:

        encrypt_hdr = {
            'content-type': 'application/json' if is_json is True else 'text/plain'
        }

        _rsp = self.post_raw('encrypt', headers=encrypt_hdr, data=json.dumps(data) if is_json is True else data)

        if _rsp.ok is True:
            if is_json is True:
                return _rsp.json()
            else:
                return _rsp.text

        else:
            raise ValueError(f"Could not complete Encryption API request. {_rsp.status_code} - {_rsp.text}.")


class OrchestratorApi(HttpClient):

    API_IDENTIFIER = 'syrup'
    API_SUFFIX = 'orchestrator/'

    def __init__(self, region_specifier: str, sapi_token: str, url: str = None) -> None:

        _base_url = urljoin(build_api_url(self.API_IDENTIFIER, region_specifier, url), self.API_SUFFIX)
        _base_hdr = {'x-storageapi-token': sapi_token}

        super().__init__(base_url=_base_url, auth_header=_base_hdr)

    def update_orchestration(self, orchestration_id: str, **kwargs) -> requests.Response:

        return self.put_raw(f'orchestrations/{orchestration_id}', json=kwargs)

    def get_orchestration(self, orchestration_id: str) -> requests.Response:

        return self.get_raw(f'orchestrations/{orchestration_id}')

    def get_orchestration_last_job(self, orchestration_id: str):

        return self.get_raw(f'orchestrations/{orchestration_id}/jobs', params={'limit': 1})

    def update_orchestration_tasks(self, orchestration_id: str, tasks: list) -> requests.Response:

        return self.put_raw(f'orchestrations/{orchestration_id}/tasks', json=tasks)


class DockerRunnerApi(HttpClient):

    API_IDENTIFIER = 'syrup'
    API_SUFFIX = 'docker'

    def __init__(self, region_specifier: str, sapi_token: str, url: str = None) -> None:

        _base_url = urljoin(build_api_url(self.API_IDENTIFIER, region_specifier, url), self.API_SUFFIX)
        _base_hdr = {'x-storageapi-token': sapi_token}

        super().__init__(base_url=_base_url, auth_header=_base_hdr)

    def create_job(self, component_id: str, configuration_id: str, configuration_row_id: str = None,
                   config_data: dict = None, variable_value_id: str = None,
                   variable_values_data: dict = None) -> requests.Response:

        job_data = {}
        job_data['config'] = configuration_id

        if configuration_row_id is not None:
            job_data['row'] = configuration_row_id

        if config_data is not None:
            job_data['configData'] = config_data

        if variable_value_id is not None:
            job_data['variableValuesId'] = variable_value_id

        if variable_values_data is not None:
            job_data['variableValuesData'] = variable_values_data

        return self.post_raw(f'{component_id}/run', json=job_data)

    def pool_job(self, job_url: str, wait_time: int = None):

        WAIT_TIME = 60 if not wait_time else wait_time
        job_is_finished = False
        counter = 0

        while job_is_finished is False:

            job_rsp = self.get_raw(job_url, is_absolute_path=True)
            job_rsp_js = job_rsp.json()

            job_status = job_rsp_js['status']

            if counter % 10 == 0:
                print(f'Pool #{counter} for job {job_url.split("/")[-1]} - status: {job_status}.')

            if job_status not in ['success', 'error', 'terminated']:
                time.sleep(WAIT_TIME)
                counter += 1
            else:
                print(f'Job {job_url} finished - status {job_status}.')
                return job_status, job_rsp_js
