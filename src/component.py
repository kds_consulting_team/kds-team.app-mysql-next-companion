import copy
import csv
import dataclasses
import json
import logging
import operator
import os
import re
import sys
import time
from dataclasses import dataclass
from pathlib import Path

import pymysql.cursors
import snowflake.connector
from deprecated import deprecated
from kbcstorage.client import Client
from keboola.component import CommonInterface
from keboola.component.base import UserException

from keboola_api import DockerRunnerApi, EncryptionApi, StorageApi, OrchestratorApi

# Basic mode setting
KEY_MODE = 'mode'
KEY_MYSQL = 'mysql'
KEY_SNFK = 'snowflake'
KEY_PARS = 'parameters'

# Database settings present in KEY_MYSQL and KEY_SNFK
KEY_HOST = 'host'
KEY_ACCT = 'account'
KEY_PORT = 'port'
KEY_USER = 'username'
KEY_PASS = '#password'
KEY_WRHS = 'warehouse'
KEY_ROLE = 'role'

# Fetch Tables
KEY_CDC_CONFIG = 'cdc_configuration'
KEY_TABLES_IN_CONFIG = 'tables_in_configuration'
KEY_INDEX_CONFIGURATIONS = 'index_configurations'
KEY_ORCHESTRATION_ADD = 'add_to_orchestration'
KEY_ORCHESTRATION_ID = 'add_to_orchestration_id'
KEY_ORCHESTRATION_PHASE = 'add_to_orchestration_phase'
KEY_SCHEMAS = 'schemas'

KEY_SIDE_BACKFILL = 'side_backfill'
KEY_SIDE_BACKFILL_USE = 'use'
KEY_SIDE_BACKFILL_ROWS = 'rows'
KEY_SIDE_BACKFILL_SIZE = 'size'

# Sync Tables
KEY_SOURCE_BUCKET = 'source_bucket'
KEY_DESTINATION_BUCKET = 'destination_bucket'

# Historize Tables
KEY_QUERY = 'query'
KEY_SCHEMA_RAW = 'schema_raw'
KEY_SCHEMA_VIEW = 'schema_view'
KEY_SCHEMA_HISTORY = 'schema_history'
KEY_SCHEMA_BACKUP = 'schema_backup'
KEY_SCHEMA_INACTIVE = 'schema_inactive'
KEY_SCHEMA_TMP = 'schema_tmp'
KEY_CDC_ORCHESTRATION_ID = 'cdc_orchestration_id'
KEY_HISTORIZE_IN_STORAGE = 'historize_in_storage'

# Both
KEY_EXCLUDE_TABLES = 'exclude_tables'
KEY_SAPI_TOKEN = '#sapi_token'

MANDATORY_PARS_FETCH_TABLES = [KEY_SCHEMAS, KEY_CDC_CONFIG, KEY_INDEX_CONFIGURATIONS,
                               KEY_TABLES_IN_CONFIG, KEY_SAPI_TOKEN]
MANDATORY_PARS_SYNC_TABLES = [KEY_SCHEMAS, KEY_SOURCE_BUCKET, KEY_DESTINATION_BUCKET, KEY_SAPI_TOKEN]
MANDATORY_PARS_HISTORIZE_TABLES = [KEY_QUERY, KEY_SAPI_TOKEN, KEY_SCHEMA_RAW, KEY_SCHEMA_VIEW, KEY_SCHEMA_HISTORY,
                                   KEY_SCHEMA_BACKUP, KEY_SCHEMA_INACTIVE, KEY_SCHEMA_TMP, KEY_CDC_ORCHESTRATION_ID]
MANDATORY_PARS_REMOVE_TABLES = [KEY_SCHEMAS, KEY_EXCLUDE_TABLES, KEY_SAPI_TOKEN]

MANDATORY_PARS_SNFK = [KEY_ACCT, KEY_USER, KEY_PASS, KEY_WRHS]
MANDATORY_PARS_MYSQL = [KEY_HOST, KEY_PORT, KEY_USER, KEY_PASS]

MYSQL_MODES = ['fetch_new_tables', 'sync_tables', 'remove_obsolete_tables']
SNFK_MODES = ['sync_tables', 'historize_tables']

KEY_STACKID = 'KBC_STACKID'
KEY_TOKEN = 'KBC_TOKEN'
KEY_PROJECT = 'KBC_PROJECTID'
KEY_RUNID = 'KBC_RUNID'

# configuration variables
KEY_DEBUG = 'debug'

# list of mandatory parameters => if some is missing, component will fail with readable message on initialization.
MANDATORY_PARS = [KEY_MODE, KEY_PARS]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.4.11'
# sys.tracebacklimit = 3

COMPONENT_ID = 'kds-team.ex-mysql-next'


@dataclass
class SnowflakeCredentials:
    host: str
    warehouse: str
    username: str
    password: str
    role: str
    cursor: snowflake.connector.cursor = snowflake.connector.DictCursor


@dataclass
class MySQLCredentials:
    host: str
    port: int
    username: str
    password: str


@dataclass
class KBCEnvironment:
    stack_id: str
    project_id: str
    run_id: str


@dataclass
class SideLoadParameters:
    use: bool
    rows: int
    size: float


@dataclass
class FetchTablesParameters:
    index_configs: bool
    nr_tables_in_config: int
    exclude_tables: list
    cdc_config: dict
    orchestration_add: bool
    orchestration_add_id: str
    orchestration_add_phase: str
    schemas: list
    sapi_token: str
    side_load: SideLoadParameters


@dataclass
class SyncTablesParameters:
    exclude_tables: list
    source_bucket: str
    destination_bucket: str
    schemas: list
    sapi_token: str


@dataclass
class HistorizeTablesParameters:
    query: str
    schema_raw: str
    schema_view: str
    schema_history: str
    schema_backup: str
    schema_inactive: str
    schema_tmp: str
    sapi_token: str
    orchestration_id: str
    historize_in_storage: bool


@dataclass
class RemoveObsoleteTablesParameters:
    schemas: list
    exclude_tables: list
    sapi_token: str
    index_configs: bool


@dataclass
class MySQLTable:
    name: str
    schema: str
    full_name: str
    state_name: str
    rows: str
    size_mb: str


class Component(CommonInterface):
    SNFK_CURSOR_TYPE = snowflake.connector.DictCursor
    DATATYPES_MAPPING = {
        'datetime': 'datetime',
        'smallint': 'smallint',
        'tinyint': 'tinyint',
        'int': 'integer',
        'text': 'text',
        'date': 'date',
        'double': 'double',
        'integer': 'integer',
        'json': 'variant',
        'longtext': 'text',
        'timestamp': 'timestamp',
        'varchar': 'varchar',
        'char': 'text',
        'bigint': 'bigint'
    }

    def __init__(self):
        default_data_dir = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
            if not os.environ.get('KBC_DATADIR') else None

        super().__init__(data_folder_path=default_data_dir)

        logging.info(f'Running version {APP_VERSION}...')

        if self.configuration.parameters.get(KEY_DEBUG, False) is True:
            logging.getLogger().setLevel(logging.DEBUG)
            sys.tracebacklimit = 3

        try:
            # validation of mandatory parameters. Produces ValueError
            self.validate_configuration(MANDATORY_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

        self.mode = self.configuration.parameters[KEY_MODE]
        self.kbc = KBCEnvironment(os.environ.get(KEY_STACKID, 'connection.keboola.com'), os.environ.get(KEY_PROJECT),
                                  os.environ.get(KEY_RUNID, '@@@123'))

        if self.mode in MYSQL_MODES:

            try:
                self.validate_parameters(self.configuration.parameters.get(KEY_MYSQL, {}),
                                         MANDATORY_PARS_MYSQL, 'mysql parameters')
            except ValueError as e:
                logging.exception(e)
                exit(1)

            self.mysql = MySQLCredentials(self.configuration.parameters[KEY_MYSQL][KEY_HOST],
                                          self.configuration.parameters[KEY_MYSQL][KEY_PORT],
                                          self.configuration.parameters[KEY_MYSQL][KEY_USER],
                                          self.configuration.parameters[KEY_MYSQL][KEY_PASS])

        else:
            self.mysql = None

        if self.mode in SNFK_MODES:

            try:
                self.validate_parameters(self.configuration.parameters.get(KEY_SNFK, {}),
                                         MANDATORY_PARS_SNFK, 'snowflake parameters')
            except ValueError as e:
                logging.exception(e)
                exit(1)

            self.snfk = SnowflakeCredentials(self.configuration.parameters[KEY_SNFK][KEY_ACCT],
                                             self.configuration.parameters[KEY_SNFK][KEY_WRHS],
                                             self.configuration.parameters[KEY_SNFK][KEY_USER],
                                             self.configuration.parameters[KEY_SNFK][KEY_PASS],
                                             self.configuration.parameters[KEY_SNFK].get(KEY_ROLE),
                                             snowflake.connector.DictCursor)

        else:
            self.snfk = None

        companion_parameters = self.configuration.parameters.get(KEY_PARS, {})

        if self.mode == 'fetch_new_tables':
            try:
                self.validate_parameters(companion_parameters, MANDATORY_PARS_FETCH_TABLES, 'config parameters')
            except ValueError as e:
                logging.exception(e)
                exit(1)

            side_backfill_params = companion_parameters.get(KEY_SIDE_BACKFILL, {})

            side_load = SideLoadParameters(side_backfill_params.get(KEY_SIDE_BACKFILL_USE, False),
                                           side_backfill_params.get(KEY_SIDE_BACKFILL_ROWS),
                                           side_backfill_params.get(KEY_SIDE_BACKFILL_SIZE))

            self.parameters = FetchTablesParameters(companion_parameters[KEY_INDEX_CONFIGURATIONS],
                                                    companion_parameters[KEY_TABLES_IN_CONFIG],
                                                    companion_parameters.get(KEY_EXCLUDE_TABLES, []),
                                                    companion_parameters[KEY_CDC_CONFIG],
                                                    companion_parameters.get(KEY_ORCHESTRATION_ADD, False),
                                                    companion_parameters.get(KEY_ORCHESTRATION_ID),
                                                    companion_parameters.get(KEY_ORCHESTRATION_PHASE, 'New phase'),
                                                    companion_parameters[KEY_SCHEMAS],
                                                    companion_parameters[KEY_SAPI_TOKEN],
                                                    side_load)

            if self.parameters.orchestration_add is True and self.parameters.orchestration_add_id is None:
                logging.error("Orchestration ID must be specified, when \"add_to_orchestration\" is set to true.")
                sys.exit(1)

        elif self.mode == 'sync_tables':

            try:
                self.validate_parameters(companion_parameters, MANDATORY_PARS_SYNC_TABLES, 'config parameters')
            except ValueError as e:
                logging.exception(e)
                exit(1)

            self.parameters = SyncTablesParameters(companion_parameters.get(KEY_EXCLUDE_TABLES, []),
                                                   companion_parameters[KEY_SOURCE_BUCKET],
                                                   companion_parameters[KEY_DESTINATION_BUCKET],
                                                   companion_parameters[KEY_SCHEMAS],
                                                   companion_parameters[KEY_SAPI_TOKEN])

        elif self.mode == 'historize_tables':

            try:
                self.validate_parameters(companion_parameters, MANDATORY_PARS_HISTORIZE_TABLES, 'config parameters')
            except ValueError as e:
                logging.exception(e)
                exit(1)

            self.parameters = HistorizeTablesParameters(
                companion_parameters[KEY_QUERY],
                companion_parameters[KEY_SCHEMA_RAW],
                companion_parameters[KEY_SCHEMA_VIEW],
                companion_parameters[KEY_SCHEMA_HISTORY],
                companion_parameters[KEY_SCHEMA_BACKUP],
                companion_parameters[KEY_SCHEMA_INACTIVE],
                companion_parameters[KEY_SCHEMA_TMP],
                companion_parameters[KEY_SAPI_TOKEN],
                companion_parameters[KEY_CDC_ORCHESTRATION_ID],
                companion_parameters.get(KEY_HISTORIZE_IN_STORAGE, False))

        elif self.mode == 'remove_obsolete_tables':

            try:
                self.validate_parameters(companion_parameters, MANDATORY_PARS_REMOVE_TABLES, 'config parameters')

            except ValueError as e:
                logging.exception(e)
                exit(1)

            self.parameters = RemoveObsoleteTablesParameters(
                companion_parameters[KEY_SCHEMAS],
                companion_parameters[KEY_EXCLUDE_TABLES],
                companion_parameters[KEY_SAPI_TOKEN],
                True
            )

        else:
            logging.error(f"Mode {self.mode} not supported.")
            exit(1)

    def _log_query(self, query):
        logging.info(f"Running query: {query}", extra={"full_message": query})

    def _mysql_create_cursor(self):
        self.mysql_conn = pymysql.connect(host=self.mysql.host,
                                          user=self.mysql.username,
                                          password=self.mysql.password,
                                          port=self.mysql.port,
                                          cursorclass=pymysql.cursors.DictCursor)

    def mysql_fetch_all_tables(self, cursor):

        all_tables = {}
        if len(self.parameters.schemas) == 0:
            logging.error("No schema provided.")
            exit(1)

        elif len(self.parameters.schemas) > 1:
            logging.error("Only 1 schema can be provided at the moment.")
            exit(1)

        else:
            pass

        schemas_str = ','.join([f'\'{x}\'' for x in self.parameters.schemas])

        sql = 'SELECT *, round(((data_length + index_length) / 1024 / 1024), 2) AS TABLE_SIZE ' + \
              f'FROM information_schema.tables WHERE table_schema IN ({schemas_str});'
        cursor.execute(sql)

        result = cursor.fetchall()

        if list(result) == []:
            logging.warning(f"No tables found in schemas: {self.parameters.schemas}.")
            exit(1)

        else:
            for table in result:
                schema = table['TABLE_SCHEMA']
                name = table['TABLE_NAME']
                if schema not in all_tables:
                    all_tables[schema] = []

                table_dao = MySQLTable(name, schema, f'{schema}.{name}', f'{schema}-{name}',
                                       table['TABLE_ROWS'], float(table['TABLE_SIZE']))

                all_tables[schema] += [table_dao]

            return all_tables

    def _mysql_get_primary_key_of_table(self, cursor, schema, table_name):
        sql = f'SHOW KEYS FROM {schema}.{table_name} WHERE key_name = \'PRIMARY\';'
        cursor.execute(sql)

        result = cursor.fetchall()

        return [c['Column_name'] for c in result]

    def get_all_configurations(self, sapi_client):
        all_configs_rsp = sapi_client.get_all_configurations(COMPONENT_ID)

        if all_configs_rsp.status_code == 200:
            return all_configs_rsp.json()

        else:
            logging.error(f"Could not download configurations of {COMPONENT_ID}. Received: {all_configs_rsp.json()}")
            exit(1)

    def parse_configurations(self, configurations, skip_index=False):

        max_index = 0
        all_configurations = {}
        for configuration in configurations:
            configuration_id = configuration['id']
            configuration_config = configuration['configuration']
            configuration_name = configuration['name']
            configuration_state = configuration['state']

            if configuration_name.startswith('00 -') or 'TMP' in configuration_name:
                continue

            if configuration_config.get('parameters') is None:
                continue

            _tables_json = json.loads(configuration_config['parameters'].get('inputMappingsJson', '{}'))
            schemas = list(_tables_json.keys())

            tables = []
            for key, values in _tables_json.items():
                tables += [f'{key}.{table}' for table in [list(x.keys())[0] for x in values['tables']]]

            _result_dict = {}
            _result_dict['id'] = configuration_id
            _result_dict['name'] = configuration_name
            _result_dict['api'] = configuration
            _result_dict['tables_json'] = _tables_json
            _result_dict['tables_json_str'] = configuration_config['parameters'].get('inputMappingsJson', '{}')
            _result_dict['tables'] = tables
            _result_dict['schemas'] = schemas
            _result_dict['state'] = configuration_state

            if not skip_index:
                if self.parameters.index_configs is True:
                    try:
                        index = int(configuration_name.split('-')[0])
                        _result_dict['index'] = index
                        if index > max_index:
                            max_index = index
                    except ValueError as e:
                        logging.exception(f"Could not get index of configuration from name {configuration_name}. "
                                          f"Turn off \"index_configurations\" option or fix name of the confgiuration."
                                          f"\n{e}")
                        sys.exit(1)
                else:
                    _result_dict['index'] = None

            else:
                _result_dict['index'] = None

            all_configurations[configuration_id] = _result_dict

        return all_configurations, max_index

    def get_and_parse_configurations(self, sapi_client, skip_index=False):
        configs = self.get_all_configurations(sapi_client)
        return self.parse_configurations(configs, skip_index)

    def _parse_update_response(self, update_response):
        tables_input_mapping = update_response['configuration']['parameters']['inputMappingsJson']
        tables_json = json.loads(tables_input_mapping)

        total_tables = 0

        for _, item in tables_json.items():
            total_tables += len(item['tables'])

        return total_tables, update_response['name']

    def get_free_configurations(self, configurations, desired_tables_in_config):

        schema = self.parameters.schemas

        free_space = []
        for id, config in configurations.items():
            if len(config['tables']) < desired_tables_in_config and config['schemas'] == schema:
                free_space += [id]

        return free_space

    def add_tables_to_configuration(self, configuration, desired_tables_in_config):

        tables_in_config = configuration.get('tables', [])
        tables_json = configuration.get('tables_json', {})
        room_left = desired_tables_in_config - len(tables_in_config)
        tables_to_add = self.to_sync[0:room_left]

        for db_table in tables_to_add:
            # db_table_split = db_table.split('.')
            db, table = db_table.schema, db_table.name

            if db not in tables_json:
                tables_json[db] = {'tables': []}

            tables_json[db]['tables'] += [{table: {'selected': True,
                                                   'replication-method': 'log_based', 'columns': {}}}]
            tables_in_config += [db_table.full_name]
            self.to_sync.remove(db_table)

        return tables_json, {'tables': tables_in_config, 'new': [t.full_name for t in tables_to_add]}

    def create_configuration_name(self, index, tables):
        if self.parameters.index_configs is True:
            config_name = f"{str(index).zfill(2)} - {', '.join(tables)}"
        else:
            config_name = ', '.join(tables)

        return config_name

    def update_configurations_with_new_tables(self, sapi_client: StorageApi, configurations,
                                              free_configurations, states=None):

        for configuration_id in free_configurations:

            if len(self.to_sync) == 0:
                logging.info("All tables synced.")
                return

            config = configurations[configuration_id]
            config_config = config['api']['configuration']
            config_index = config['index']

            tables_json, tables_configuration = self.add_tables_to_configuration(
                config, self.parameters.nr_tables_in_config)
            tables_in_config, added_tables = tables_configuration['tables'], tables_configuration['new']
            config_config['parameters']['inputMappingsJson'] = json.dumps(tables_json, indent=2)

            config_name = self.create_configuration_name(config_index, tables_in_config)

            updated_config = {'name': config_name,
                              'configuration': json.dumps(config_config),
                              'changeDescription': f"Added tables {', '.join(added_tables)}"}

            if states:
                current_state = copy.deepcopy(config['state'])

                if 'component' not in current_state:
                    current_state['component'] = {
                        'currently_syncing': None,
                        'bookmarks': {}
                    }

                for table in added_tables:
                    table_state_name = self.state_map[table]
                    if table_state_name not in states:
                        continue
                    else:
                        current_state['component']['bookmarks'][table_state_name] = states[table_state_name]

                updated_config['state'] = json.dumps(current_state)

            update_config_rsp = sapi_client.update_configuration(COMPONENT_ID, configuration_id,
                                                                 config_json=updated_config)

            if update_config_rsp.status_code == 200:
                new_tables_len, new_name = self._parse_update_response(update_config_rsp.json())

                if new_tables_len != len(tables_in_config) or new_name != config_name:
                    logging.error(f"Update for {configuration_id} was not succesful.")
                    exit(1)

                logging.info(
                    f"Successfully updated configuration {configuration_id} with new "
                    f"tables {', '.join(added_tables)}.")

            else:
                logging.error(f"Could not update configuration {configuration_id}. "
                              f"Received: {update_config_rsp.status_code} - {update_config_rsp.json()}")
                exit(1)

    def create_configurations_with_new_tables(self, sapi_client: StorageApi, encrypt_client: EncryptionApi,
                                              orchestrator_client: OrchestratorApi, index, states=None):

        while len(self.to_sync) > 0:
            tables_json, tables = self.add_tables_to_configuration({}, self.parameters.nr_tables_in_config)

            index += 1

            configuration = self.parameters.cdc_config
            configuration['inputMappingsJson'] = json.dumps(tables_json, indent=2)
            configuration['host'] = self.mysql.host
            configuration['port'] = self.mysql.port
            configuration['username'] = self.mysql.username
            configuration['#password'] = self.mysql.password

            configuration_encrypted = encrypt_client.encrypt_data(configuration)
            config_name = self.create_configuration_name(index, tables['tables'])

            config = {'name': config_name, 'configuration': json.dumps({'parameters': configuration_encrypted})}
            if states:
                current_state = {
                    'component': {
                        'currently_syncing': None,
                        'bookmarks': {}
                    }
                }

                for table in tables['tables']:
                    table_state_name = self.state_map[table]
                    if table_state_name not in states:
                        continue
                    else:
                        current_state['component']['bookmarks'][table_state_name] = states[table_state_name]

                config['state'] = json.dumps(current_state)

            new_config_rsp = sapi_client.create_new_configuration(COMPONENT_ID, config)

            if new_config_rsp.status_code == 201:
                logging.info(f"Created new configuration for tables {', '.join(tables['tables'])}.")
                new_config_id = new_config_rsp.json()['id']

                if self.parameters.orchestration_add is True:
                    self.update_orchestration_tasks(
                        orchestrator_client, self.parameters.orchestration_add_id, new_config_id)
            else:
                logging.error("Could not create new configuration."
                              f"Received: {new_config_rsp.status_code} - {new_config_rsp.json()}.")
                exit(1)

    def update_orchestration_tasks(self, orchestrator_client: OrchestratorApi, orchestration_id, configuration_id):

        orch_rsp = orchestrator_client.get_orchestration(orchestration_id)

        if orch_rsp.ok is not True:
            logging.exception(f"Could not retrieve orchestration {orchestration_id}. "
                              f"Received: {orch_rsp.status_code} - {orch_rsp.json()}.")
            exit(1)

        tasks = orch_rsp.json()['tasks']

        new_cfg_task = {
            "component": COMPONENT_ID,
            "action": "run",
            "actionParameters": {
                "config": str(configuration_id)
            },
            "continueOnFailure": False,
            "timeoutMinutes": None,
            "active": True,
            "phase": self.parameters.orchestration_add_phase
        }

        new_tasks = []
        encountered = False
        written = False

        for task in tasks:
            if encountered is False or (encountered is True and written is True) or \
                    (encountered is True and written is False and task['phase'] ==
                     self.parameters.orchestration_add_phase):
                if task['phase'] == self.parameters.orchestration_add_phase and encountered is False:
                    encountered = True
                new_tasks += [task]
            elif encountered is True and written is False and task['phase'] != self.parameters.orchestration_add_phase:
                new_tasks += [new_cfg_task]
                new_tasks += [task]
                written = True
            else:
                raise ValueError("Logic missing.")

        if written is False:
            new_tasks += [new_cfg_task]

        update_orch_rsp = orchestrator_client.update_orchestration_tasks(orchestration_id, tasks=new_tasks)

        if update_orch_rsp.ok is True:
            logging.info(f"Updated orchestration {orchestration_id} with new configuration {configuration_id}.")
        else:
            logging.error(f"Could not update {orchestration_id}. "
                          f"Received: {update_orch_rsp.status_code} - {update_orch_rsp.json()}.")
            exit(1)

    def enable_orchestration(self, orchestrator_client: OrchestratorApi, orchestration_id):

        enable_orch_rsp = orchestrator_client.update_orchestration(orchestration_id, active=True)

        if enable_orch_rsp.ok is True:
            logging.info(f"Orchestration {orchestration_id} enabled successfully.")
        else:
            logging.error(f"Could not enable orchestration {orchestration_id}.")
            sys.exit(1)

    def disable_orchestration(self, orchestrator_client: OrchestratorApi, orchestration_id):

        disable_orch_rsp = orchestrator_client.update_orchestration(orchestration_id, active=False)

        if disable_orch_rsp.ok is True:
            logging.info(f"Orchestration {orchestration_id} disabled successfully.")
        else:
            logging.error(f"Could not disable orchestration {orchestration_id}.")
            sys.exit(1)

    def create_api_clients(self, sapi_token: str):

        self.sapi = StorageApi('', sapi_token, url=self.kbc.stack_id.replace('connection.', ''))
        self.encr = EncryptionApi('', COMPONENT_ID, self.kbc.project_id,
                                  url=self.kbc.stack_id.replace('connection.', ''))
        self.orch = OrchestratorApi('', sapi_token, url=self.kbc.stack_id.replace('connection.', ''))
        self.kbc_storage = Client(f'https://{self.kbc.stack_id}', sapi_token)
        self.docker = DockerRunnerApi('', sapi_token, url=self.kbc.stack_id.replace('connection.', ''))

    def get_and_parse_bucket(self, bucket_id: str, include_metadata: bool = True):

        out = dict()
        bucket_rsp = self.sapi.get_tables_in_bucket(bucket_id, include='columns,columnMetadata')

        if bucket_rsp.ok is True:
            bucket_data = bucket_rsp.json()
        else:
            logging.error(f"Could not download data about bucket {bucket_id}: {bucket_rsp.json()}")
            sys.exit(1)

        try:
            skip_tables = self.parameters.exclude_tables
        except AttributeError:
            skip_tables = []

        for table in bucket_data:
            name = table['name']
            columns = table['columns']
            metadata = table['columnMetadata']

            if name in skip_tables:
                continue

            column_datatypes = dict()

            if include_metadata is True:

                if not metadata:
                    raise UserException(
                        f'The source table {table["id"]} does not have column metadata defined. '
                        f'Please set the metadata first')

                for column, md in metadata.items():
                    for md_item in md:
                        if md_item['key'] == 'KBC.datatype.type' and md_item['provider'] == COMPONENT_ID:
                            column_datatypes[column] = md_item['value']
                        else:
                            continue

            out[name] = {'columns': columns, 'dt': column_datatypes, 'md': metadata}

        return out

    def determine_tables_to_sync(self, source_bucket, destination_bucket):

        new_tables = []
        table_changes = {}

        for table in source_bucket:
            if table in self.parameters.exclude_tables:
                continue
            else:
                if table not in destination_bucket:
                    new_tables += [table]
                    continue

                missing_cols = []
                extra_cols = []
                wrong_dt = []

                src_cols = source_bucket[table]['columns']
                dst_cols = destination_bucket[table]['columns']

                for col in src_cols:
                    if col not in dst_cols:
                        missing_cols += [col]
                        continue

                    col_src_dt = source_bucket[table]['dt'][col]
                    col_dst_dt = destination_bucket[table]['dt'][col]

                    if col_src_dt != col_dst_dt:
                        wrong_dt += [col]

                for col in dst_cols:
                    if col not in src_cols:
                        extra_cols += [col]

                create_record = any([(ms := len(missing_cols)) > 0, (es := len(extra_cols)) > 0,
                                     (wdt := len(wrong_dt)) > 0])

                if create_record:
                    table_changes[table] = dict()

                if ms > 0:
                    table_changes[table]['add_columns'] = missing_cols

                if es > 0:
                    table_changes[table]['drop_columns'] = extra_cols

                if wdt > 0:
                    table_changes[table]['sync_datatypes'] = wrong_dt

        return new_tables, table_changes

    def _snfk_create_cursor(self):

        self.snfk_conn = snowflake.connector.connect(user=self.snfk.username, password=self.snfk.password,
                                                     account=self.snfk.host,
                                                     database=f'SAPI_{self.kbc.project_id}',
                                                     warehouse=self.snfk.warehouse,
                                                     session_parameters={
                                                         'QUERY_TAG': f'{{"runId":"{self.kbc.run_id}"}}'
                                                     })

    def snfk_drop_view_and_recreate_table(self, cursor: snowflake.connector.cursor, schema: str,
                                          object: str, columns: list, empty_table: bool = False):

        columns_string = ",".join([f'"{c}" VARCHAR' if empty_table else f'"{c}"' for c in columns])

        if empty_table:
            create_tmp_table_sql = f'CREATE OR REPLACE TABLE "{schema}"."__{object}_tmp__" ({columns_string});'

        else:
            create_tmp_table_sql = f'CREATE OR REPLACE TABLE "{schema}"."__{object}_tmp__" AS ' + \
                                   f'SELECT {columns_string} FROM "{schema}"."{object}";'

        self._log_query(create_tmp_table_sql)

        cursor.execute(create_tmp_table_sql)
        grants = self.snfk_generate_grant_commands(cursor, schema, object)

        drop_view_sql = f'DROP VIEW "{schema}"."{object}";'
        create_table_sql = f'CREATE TABLE "{schema}"."{object}" CLONE "{schema}"."__{object}_tmp__";'
        drop_tmp_table_sql = f'DROP TABLE IF EXISTS "{schema}"."__{object}_tmp__";'

        self._log_query(drop_view_sql)
        cursor.execute(drop_view_sql)

        self._log_query(create_table_sql)
        cursor.execute(create_table_sql)

        self._log_query(drop_tmp_table_sql)
        cursor.execute(drop_tmp_table_sql)

        for g in grants:
            self._log_query(g)
            cursor.execute(g)

    def snfk_drop_table_and_recreate_view(self, cursor: snowflake.connector.cursor, schema: str,
                                          object: str, view_query: str):

        grants = self.snfk_generate_grant_commands(cursor, schema, object)

        drop_table_sql = f'DROP TABLE "{schema}"."{object}";'
        self._log_query(drop_table_sql)
        cursor.execute(drop_table_sql)

        self._log_query(view_query)
        cursor.execute(view_query)

        for g in grants:
            self._log_query(g)
            cursor.execute(g)

    def snfk_generate_grant_commands(self, cursor, schema, object, dst_schema=None):

        if dst_schema is None:
            dst_schema = schema

        get_grants_sql = f'SHOW GRANTS ON "{schema}"."{object}";'
        cursor.execute(get_grants_sql)

        grants = cursor.fetchall()

        grant_sql = 'GRANT {privilege} ON "{schema}"."{object}" TO {granted_to} {grantee_name} {revoke};'

        grant_ownership = [grant_sql.format(**x, schema=dst_schema,
                                            object=object, revoke='REVOKE CURRENT GRANTS')
                           for x in grants if x['privilege'] == 'OWNERSHIP']
        grant_other = [grant_sql.format(**x, schema=dst_schema,
                                        object=object, revoke='') for x in grants if x['privilege'] != 'OWNERSHIP']

        return grant_ownership + grant_other

    def map_snfk_table_to_mysql(self, mysql_tables, snfk_table_name):

        tables = [t.name for t in mysql_tables[self.parameters.schemas[0]]]

        for table in tables:
            if table.upper() == snfk_table_name.upper():
                return table

    def create_csv(self, columns):

        if os.path.exists(self.data_folder_path) is False:
            os.makedirs(self.data_folder_path)

        tmp_dir = os.path.join(self.data_folder_path, 'tmp')

        if os.path.exists(tmp_dir) is False:
            os.makedirs(tmp_dir)

        csv_path = os.path.join(tmp_dir, 'tmp.csv')

        with open(csv_path, 'w') as csv_io:
            wrt = csv.DictWriter(csv_io, fieldnames=columns, quoting=csv.QUOTE_ALL)
            wrt.writeheader()

        return csv_path

    def create_view_query(self, destination_bucket, source_bucket, name, columns_string, primary_key):

        VIEW_STATEMENT = '''
        CREATE OR REPLACE VIEW "{dst}"."{name}" AS
        WITH view AS (
        SELECT {cols}, "_timestamp"
        FROM "{src}"."{name}"
        QUALIFY ROW_NUMBER() OVER (PARTITION BY {pk} ORDER BY
        "BINLOG_CHANGE_AT"::INT DESC, "BINLOG_READ_AT"::INT DESC) = 1)
        SELECT *
        FROM view
        WHERE KBC_DELETED_AT IS NULL;'''.format(dst=destination_bucket, src=source_bucket, name=name,
                                                cols=columns_string, pk=primary_key)

        return VIEW_STATEMENT

    def create_new_tables(self, mysql_cursor, snowflake_cursor, keboola_tables, mysql_tables, new_tables):

        for new in new_tables:

            logging.info(f"Creating new table {new}...")

            table_in_mysql = self.map_snfk_table_to_mysql(mysql_tables, new)

            if table_in_mysql is None:
                logging.error(f"Could not find table {new} in MySQL. Could not obtain primary key.")
                exit(1)

            table_pk = self._mysql_get_primary_key_of_table(mysql_cursor, self.parameters.schemas[0], table_in_mysql)

            if table_pk == [] or table_pk is None:
                logging.error(f"Table {table_in_mysql} in MySQL has no primary key.")
                exit(1)

            table_columns = keboola_tables[new]['columns']
            table_metadata = keboola_tables[new]['md']
            table_datatypes = keboola_tables[new]['dt']

            column_queries = []

            for column in table_columns:
                datatype = re.sub(r'\(\d*\)', '', table_datatypes[column])
                snfk_datatype = self.DATATYPES_MAPPING.get(datatype.lower(), datatype.lower())
                column_queries += [f"IFF(\"{column}\" = '', NULL, "
                                   f"\"{column}\"::{snfk_datatype}) "
                                   f" AS \"{column}\""]

            columns_string = ',\n'.join(column_queries)
            view_query = self.create_view_query(self.parameters.destination_bucket, self.parameters.source_bucket, new,
                                                columns_string, ','.join(table_pk))

            csv_path = self.create_csv(table_columns)

            logging.info(f"Creating table {new} in bucket {self.parameters.destination_bucket}.")
            table_id = self.kbc_storage.tables.create(self.parameters.destination_bucket, new, csv_path)

            logging.debug(f"Created table {table_id}. Creating view...")

            self.snfk_drop_table_and_recreate_view(snowflake_cursor, self.parameters.destination_bucket,
                                                   new, view_query)

            # logging.info("Backfilling metadata...")
            # for column, md in table_metadata.items():
            #     column_id = f'{self.parameters.destination_bucket}.{new}.{column}'

            #     md_new = []
            #     for mdi in md:
            #         if mdi['key'] == 'KBC.datatype.nullable':
            #             mdi['value'] = 0
            #         md_new += [mdi]

            #     _rsp = self.sapi.create_column_metadata(column_id, COMPONENT_ID, md_new)

            #     if _rsp.ok is not True:
            #         logging.warn(column_id, _rsp.json())

            # logging.debug(f"Backfilled metadata for table {new} in bucket {self.parameters.destination_bucket}.")

            self.create_column_metadata(self.parameters.destination_bucket, new, table_metadata)

    def sync_columns(self, mysql_cursor, snowflake_cursor, mysql_tables: dict, table_changes: dict,
                     kbc_tables_src: dict, kbc_tables_dst: dict):

        for table, column_sync in table_changes.items():
            add_columns = column_sync.get('add_columns', [])
            drop_columns = column_sync.get('drop_columns', [])
            sync_datatypes = column_sync.get('sync_datatypes', [])

            to_sync_md = list(set(add_columns + sync_datatypes))

            logging.info(f"Syncing column settings for table {self.parameters.destination_bucket}.{table}.")

            table_in_mysql = self.map_snfk_table_to_mysql(mysql_tables, table)

            if not table_in_mysql:
                logging.error(f"Could not find table {table} in MySQL. Could not obtain primary key.")
                exit(1)

            table_pk = self._mysql_get_primary_key_of_table(mysql_cursor, self.parameters.schemas[0], table_in_mysql)

            if not table_pk:
                logging.error(f"Table {table_in_mysql} in MySQL has no primary key.")
                exit(1)

            table_columns = kbc_tables_src[table]['columns']
            table_metadata = kbc_tables_src[table]['md']
            table_datatypes = kbc_tables_src[table]['dt']

            # table_id_src = f"{self.parameters.source_bucket}.{table}"
            table_id_dst = f"{self.parameters.destination_bucket}.{table}"

            column_queries = []

            for column in table_columns:
                column_dt = table_datatypes.get(column)
                if not column_dt:
                    logging.error(f"Column {column} in table {table} has no metadata set.")
                    sys.exit(1)
                datatype = re.sub(r'\(\d*\)', '', table_datatypes[column])
                snfk_datatype = self.DATATYPES_MAPPING.get(datatype.lower(), datatype.lower())
                column_queries += [f"IFF(\"{column}\" = '', NULL, "
                                   f"\"{column}\"::{snfk_datatype}) "
                                   f"AS \"{column}\""]

            columns_string = ',\n'.join(column_queries)
            view_query = self.create_view_query(self.parameters.destination_bucket, self.parameters.source_bucket,
                                                table, columns_string, ','.join(table_pk))

            dst_columns = kbc_tables_dst[table]['columns']
            if len(drop_columns) == 0:
                empty_table = False
            else:
                empty_table = True
            self.snfk_drop_view_and_recreate_table(snowflake_cursor, self.parameters.destination_bucket,
                                                   table, dst_columns, empty_table=empty_table)

            for c in add_columns:
                self.add_table_column(table_id_dst, c)

            for c in drop_columns:
                self.drop_table_column(table_id_dst, c)

            logging.info("Recreating view...")
            self.snfk_drop_table_and_recreate_view(snowflake_cursor, self.parameters.destination_bucket,
                                                   table, view_query)

            self.create_column_metadata(self.parameters.destination_bucket, table, table_metadata, to_sync_md)

    def add_table_column(self, table_id, column_name):

        add_column_rsp = self.sapi.create_column(table_id, column_name)

        if add_column_rsp.ok:
            is_success = self.kbc_storage.jobs.block_for_success(add_column_rsp.json()['id'])

            if is_success:
                logging.info(f"Created column {column_name} in table {table_id}.")
            else:
                logging.error(f"Could not create column {column_name} in table {table_id}.\n"
                              f"{add_column_rsp.json()}")
                sys.exit(1)

        else:
            logging.error(f"Could not create column {column_name} for table {table_id}.\n"
                          f"{add_column_rsp.json()}")
            sys.exit(1)

    def drop_table_column(self, table_id, column_name):

        drop_column_rsp = self.sapi.drop_column(table_id, column_name)

        if drop_column_rsp.ok:
            is_success = self.kbc_storage.jobs.block_for_success(drop_column_rsp.json()['id'])

            if is_success:
                logging.info(f"Dropped column {column_name} in table {table_id}.")
            else:
                logging.error(f"Could not drop column {column_name} in table {table_id}.\n"
                              f"{drop_column_rsp.json()}")
                sys.exit(1)

        else:
            logging.error(f"Could not drop column {column_name} for table {table_id}.\n"
                          f"{drop_column_rsp.json()}")
            sys.exit(1)

    @deprecated
    def add_missing_columns(self, mysql_cursor, snowflake_cursor, keboola_tables, mysql_tables, missing_tables,
                            keboola_tables_dst):

        for table, missing_columns in missing_tables.items():

            logging.info(f"Adding missing columns in table {table}...")

            table_in_mysql = self.map_snfk_table_to_mysql(mysql_tables, table)

            if table_in_mysql is None:
                logging.error(f"Could not find table {table} in MySQL. Could not obtain primary key.")
                exit(1)

            table_pk = self._mysql_get_primary_key_of_table(mysql_cursor, self.parameters.schemas[0], table_in_mysql)

            if table_pk == [] or table_pk is None:
                logging.error(f"Table {table_in_mysql} in MySQL has no primary key.")
                exit(1)

            table_columns = keboola_tables[table]['columns']
            table_metadata = keboola_tables[table]['md']
            table_datatypes = keboola_tables[table]['dt']

            column_queries = []

            for column in table_columns:
                datatype = re.sub(r'\(\d*\)', '', table_datatypes[column])
                snfk_datatype = self.DATATYPES_MAPPING.get(datatype.lower(), datatype.lower())
                column_queries += [f"IFF({column} = '', NULL, {column}::{snfk_datatype}) "
                                   f"AS {column}"]

            columns_string = ',\n'.join(column_queries)
            view_query = self.create_view_query(self.parameters.destination_bucket, self.parameters.source_bucket,
                                                table, columns_string, ','.join(table_pk))

            dst_columns = keboola_tables_dst[table]['columns']
            self.snfk_drop_view_and_recreate_table(snowflake_cursor, self.parameters.destination_bucket,
                                                   table, dst_columns)

            for c in missing_columns:
                column_rsp = self.sapi.create_column(f"{self.parameters.destination_bucket}.{table}", c)

                if column_rsp.ok is True:
                    is_success = self.kbc_storage.jobs.block_for_success(column_rsp.json()['id'])

                    if is_success is True:
                        logging.info(f"Created column {c} in table {table}.")
                    else:
                        logging.error(f"Could not create column {c} for table {table}.")
                        sys.exit(1)

                else:
                    logging.error(f"Could not create column {c} for table {table}.\n{column_rsp.json()}")
                    sys.exit(1)

            logging.info("Recreating view...")
            self.snfk_drop_table_and_recreate_view(snowflake_cursor, self.parameters.destination_bucket,
                                                   table, view_query)

            # logging.info("Backfilling metadata...")
            # for column, md in table_metadata.items():
            #     if column not in missing_columns:
            #         continue
            #     column_id = f'{self.parameters.destination_bucket}.{table}.{column}'

            #     md_new = []
            #     for mdi in md:
            #         if mdi['key'] == 'KBC.datatype.nullable':
            #             mdi['value'] = 0
            #         md_new += [mdi]

            #     _rsp = self.sapi.create_column_metadata(column_id, COMPONENT_ID, md_new)

            #     if _rsp.ok is not True:
            #         logging.warn(column_id, _rsp.json())

            self.create_column_metadata(self.parameters.destination_bucket, table, table_metadata, missing_columns)

    def create_column_metadata(self, bucket: str, table_name: str, table_metadata: dict, columns_to_sync: list = None):

        logging.info("Backfilling metadata...")
        for column, md in table_metadata.items():
            if columns_to_sync:
                if column not in columns_to_sync:
                    continue
            column_id = f'{bucket}.{table_name}.{column}'

            md_new = []
            for mdi in md:
                if mdi['key'] == 'KBC.datatype.nullable':
                    mdi['value'] = 0
                md_new += [mdi]

            _rsp = self.sapi.create_column_metadata(column_id, COMPONENT_ID, md_new)

            if _rsp.ok is False:
                logging.warn(f"Could not create metadata for column {column_id}.\n{_rsp.json()}")

        logging.debug(f"Backfilled column metadata for table {bucket}.{table_name}.")

    def snfk_get_tables_to_historize(self, cursor: snowflake.connector.cursor):

        self._log_query(self.parameters.query)
        cursor.execute(self.parameters.query)

        tables_to_historize = [t["TABLE_NAME"] for t in cursor.fetchall()]

        logging.info(f"Following tables will be historized: {tables_to_historize}.")
        return tables_to_historize

    def snfk_get_table_columns(self, cursor: snowflake.connector.cursor, schema: str,
                               table_name: str, clean: bool = False):

        get_columns_sql = f'''
        SELECT column_name
        FROM information_schema.columns
        WHERE table_schema = '{schema}'
            and table_name = '{table_name}'
        ORDER BY ordinal_position;
        '''

        self._log_query(get_columns_sql)
        cursor.execute(get_columns_sql)

        if clean is False:
            return [f"\"{c['COLUMN_NAME']}\"" for c in cursor.fetchall()]
        else:
            return [c['COLUMN_NAME'] for c in cursor.fetchall()]

    def snfk_create_history_table(self, cursor: snowflake.connector.cursor, table_name: str):

        create_history_table_sql = f'create table if not exists "{self.parameters.schema_history}".{table_name} as' + \
                                   f'\nselect *\nfrom "{self.parameters.schema_raw}"."{table_name}"\nlimit 0;'

        self._log_query(create_history_table_sql)
        cursor.execute(create_history_table_sql)

    def snfk_create_backup_table(self, cursor: snowflake.connector.cursor, table_name: str):
        create_backup_table_sql = f'create or replace table "{self.parameters.schema_backup}".{table_name}\n' + \
                                  f'clone "{self.parameters.schema_raw}".{table_name};'

        self._log_query(create_backup_table_sql)
        cursor.execute(create_backup_table_sql)

    def snfk_create_tmp_table(self, cursor: snowflake.connector.cursor, table_name: str):
        create_tmp_table_sql = f'create or replace table "{self.parameters.schema_tmp}".{table_name} as\n' + \
                               f'select *\nfrom "{self.parameters.schema_raw}"."{table_name}"\nlimit 0;'

        self._log_query(create_tmp_table_sql)
        cursor.execute(create_tmp_table_sql)

    def snfk_fill_tmp_table_sql(self, cursor: snowflake.connector.cursor, table_name: str, columns: list):

        table_columns_str = ',\n  '.join(columns)
        table_columns_nonnull = ',\n  '.join([f'IFNULL({c}::VARCHAR, \'\') AS {c}' if c != '"_timestamp"'
                                              else c for c in columns])

        fill_tmp_table_sql = f'insert into "{self.parameters.schema_tmp}".{table_name}\n(\n  {table_columns_str}\n)' + \
                             f'\nselect {table_columns_nonnull}\nfrom "{self.parameters.schema_view}".{table_name};'

        self._log_query(fill_tmp_table_sql)
        cursor.execute(fill_tmp_table_sql)

    def snfk_get_ddl(self, cursor: snowflake.connector.cursor, table_name: str):

        get_ddl_sql = f'SELECT GET_DDL(\'view\', \'"{self.parameters.schema_view}".{table_name}\') as ddl;'
        self._log_query(get_ddl_sql)
        cursor.execute(get_ddl_sql)

        return cursor.fetchone()

    def get_pk_from_dll(self, ddl_query: str) -> str:

        pk_match = re.search(r'PARTITION BY .+ ORDER BY', ddl_query['DDL'])
        return re.sub(r'PARTITION BY | ORDER BY', '', pk_match[0])

    def snfk_create_inactive_view_sql(self, cursor: snowflake.connector.cursor, table_name: str,
                                      columns: list, pk: str):

        # columns_query = ',\n'.join([f'IFF({c} = \'\', NULL, {c}) AS {c}' if c != '"_timestamp"'
        #                             else c for c in columns])

        columns_query = ',\n'.join([f'IFNULL({c}::VARCHAR, \'\') AS {c}' if c != '"_timestamp"'
                                    else c for c in columns])

        # columns_query = ',\n'.join(columns)

        create_inactive_view_sql = f'''
        CREATE OR REPLACE VIEW "{self.parameters.schema_inactive}"."{table_name}" AS
        WITH view AS (
        SELECT {columns_query}
        FROM "{self.parameters.schema_raw}"."{table_name}"
        QUALIFY ROW_NUMBER() OVER (PARTITION BY {pk} ORDER BY
        "BINLOG_CHANGE_AT"::INT DESC, "BINLOG_READ_AT"::INT DESC) <> 1 or KBC_DELETED_AT IS NOT NULL)
        SELECT *
        FROM view;'''

        self._log_query(create_inactive_view_sql)
        cursor.execute(create_inactive_view_sql)

    def snfk_insert_inactive_rows_sql(self, cursor: snowflake.connector.cursor, table_name: str, columns: str):

        inactive_rows_sql = f'insert into "{self.parameters.schema_history}".{table_name}\n(\n  {columns}\n)\n' + \
                            f'select {columns}\nfrom "{self.parameters.schema_inactive}".{table_name};'

        self._log_query(inactive_rows_sql)
        cursor.execute(inactive_rows_sql)

    def snfk_create_grants(self, cursor: snowflake.connector.cursor, table_name: str):

        grants = self.snfk_generate_grant_commands(cursor, self.parameters.schema_raw,
                                                   table_name, self.parameters.schema_tmp)

        for g in grants:
            self._log_query(g)
            cursor.execute(g)

    def snfk_swap_tmp_table(self, cursor: snowflake.connector.cursor, table_name: str):

        swap_sql = f'alter table "{self.parameters.schema_raw}".{table_name}\n' + \
                   f'swap with "{self.parameters.schema_tmp}".{table_name};'

        self._log_query(swap_sql)
        cursor.execute(swap_sql)

    def snfk_add_missing_columns(self, cursor: snowflake.connector.cursor, schema: str, table_name: str,
                                 columns_src: list, columns_dst: list):

        for column in columns_src:
            if column not in columns_dst:
                create_columns_sql = f'ALTER TABLE "{schema}"."{table_name}"\n    ADD COLUMN {column} VARCHAR;'
                self._log_query(create_columns_sql)
                cursor.execute(create_columns_sql)

    def snfk_drop_tmp_table(self, cursor: snowflake.connector.cursor, table_name: str):

        drop_table_sql = f'DROP TABLE "{self.parameters.schema_tmp}".{table_name};'
        self._log_query(drop_table_sql)
        cursor.execute(drop_table_sql)

    def historize_table(self, cursor: snowflake.connector.cursor, table_name: str):

        table_columns = self.snfk_get_table_columns(cursor, self.parameters.schema_raw, table_name)
        self.snfk_create_history_table(cursor, table_name)
        self.snfk_create_backup_table(cursor, table_name)
        self.snfk_create_tmp_table(cursor, table_name)
        table_columns_history = self.snfk_get_table_columns(cursor, self.parameters.schema_history, table_name)
        self.snfk_add_missing_columns(cursor, self.parameters.schema_history, table_name,
                                      table_columns, table_columns_history)

        table_columns_str = ',\n  '.join(table_columns)
        self.snfk_fill_tmp_table_sql(cursor, table_name, table_columns)
        ddl = self.snfk_get_ddl(cursor, table_name)
        table_pk = self.get_pk_from_dll(ddl)

        self.snfk_create_inactive_view_sql(cursor, table_name, table_columns, table_pk)

        self.snfk_insert_inactive_rows_sql(cursor, table_name, table_columns_str)
        self.snfk_create_grants(cursor, table_name)
        self.snfk_swap_tmp_table(cursor, table_name)
        self.snfk_drop_tmp_table(cursor, table_name)

        logging.info(f"Historization for table {table_name} finished.")

    def historize_table_storage(self, cursor: snowflake.connector.cursor, table_name: str, tables_in_bucket: dict):
        table_columns = self.snfk_get_table_columns(cursor, self.parameters.schema_raw, table_name, clean=True)
        table_columns_no_ts = [t for t in table_columns if t != '_timestamp']
        table_columns_quoted = [f'\"{c}\"' for c in table_columns]

        if table_name not in tables_in_bucket:
            csv_path = self.create_csv(table_columns_no_ts)
            table_id = self.kbc_storage.tables.create(self.parameters.schema_history, table_name, csv_path)
            logging.info(f"Created new table {table_id}.")
        else:
            columns_in_storage = tables_in_bucket[table_name]['columns']
            for column in table_columns_no_ts:
                if column not in columns_in_storage:
                    logging.info(f"Creating new column {column} in {self.parameters.schema_history}.{table_name}.")
                    create_column_rsp = self.sapi.create_column(f'{self.parameters.schema_history}.{table_name}',
                                                                column)

                    if create_column_rsp.ok is True:
                        is_success = self.kbc_storage.jobs.block_for_success(create_column_rsp.json()['id'])

                        if is_success is True:
                            logging.info(f"Created column {column} in table {table_name}.")
                        else:
                            logging.error(f"Could not create column {column} for table {table_name}.")
                            sys.exit(1)

                    else:
                        logging.error(f"Could not create column {column} for table {table_name}.\n"
                                      f"{create_column_rsp.json()}")
                        sys.exit(1)

        self.snfk_create_backup_table(cursor, table_name)
        self.snfk_create_tmp_table(cursor, table_name)

        table_columns_str = ',\n  '.join(table_columns_quoted)
        self.snfk_fill_tmp_table_sql(cursor, table_name, table_columns_quoted)
        ddl = self.snfk_get_ddl(cursor, table_name)
        table_pk = self.get_pk_from_dll(ddl)

        self.snfk_create_inactive_view_sql(cursor, table_name, table_columns_quoted, table_pk)

        self.snfk_insert_inactive_rows_sql(cursor, table_name, table_columns_str)
        self.snfk_create_grants(cursor, table_name)
        self.snfk_swap_tmp_table(cursor, table_name)
        self.snfk_drop_tmp_table(cursor, table_name)

    def disable_orchestration_and_wait_for_job(self, orchestration_id):

        logging.info(f"Disabling orchestration {orchestration_id}.")
        self.disable_orchestration(self.orch, orchestration_id)

        last_job_running = True

        while last_job_running is True:
            job_rsp = self.orch.get_orchestration_last_job(orchestration_id)

            if job_rsp.ok is True:
                job_js = job_rsp.json()
                job_is_finished = job_js[0]['isFinished']
                job_id = job_js[0]['id']

                if job_is_finished is True:
                    last_job_running = False
                else:
                    logging.info(f'Last job {job_id} of orchestration is still running. Waiting 15s...')
                    time.sleep(15)

    def snfk_use_role(self, snfk_cursor):

        snfk_cursor.execute(f'USE ROLE {self.snfk.role};')

    def run_historize_tables(self):

        sapi_token = self.parameters.sapi_token
        self.create_api_clients(sapi_token)
        self._snfk_create_cursor()

        if self.parameters.historize_in_storage is True:
            all_tables = self.get_and_parse_bucket(self.parameters.schema_history, include_metadata=False)

        with self.snfk_conn:
            with self.snfk_conn.cursor(self.snfk.cursor) as snfk_cursor:
                if self.snfk.role:
                    self.snfk_use_role(snfk_cursor)
                tables_to_historize = self.snfk_get_tables_to_historize(snfk_cursor)

                if len(tables_to_historize) == 0:
                    logging.info("No tables to historize.")
                    return

                try:
                    self.disable_orchestration_and_wait_for_job(self.parameters.orchestration_id)

                    for table in tables_to_historize:
                        logging.info(f"Processing table {table}...")
                        if self.parameters.historize_in_storage is False:
                            self.historize_table(snfk_cursor, table)
                        else:
                            self.historize_table_storage(snfk_cursor, table, all_tables)

                finally:
                    logging.info(f"Enabling orchestration {self.parameters.orchestration_id}...")
                    self.enable_orchestration(self.orch, self.parameters.orchestration_id)

    def run_sync_tables(self):

        sapi_token = self.parameters.sapi_token
        self.create_api_clients(sapi_token)

        source_bucket = self.get_and_parse_bucket(self.parameters.source_bucket)
        destination_bucket = self.get_and_parse_bucket(self.parameters.destination_bucket)

        new_tables, table_changes = self.determine_tables_to_sync(source_bucket, destination_bucket)

        if len(new_tables) == 0 and len(table_changes) == 0:
            logging.info("No new tables detected. No tables need to be updated.")
            sys.exit(0)
        else:
            self._mysql_create_cursor()
            self._snfk_create_cursor()

        with self.mysql_conn, self.snfk_conn:
            with self.mysql_conn.cursor() as mysql_cursor, self.snfk_conn.cursor(self.snfk.cursor) as snfk_cursor:
                if self.snfk.role:
                    self.snfk_use_role(snfk_cursor)

                mysql_tables = self.mysql_fetch_all_tables(mysql_cursor)

                self.create_new_tables(mysql_cursor, snfk_cursor, source_bucket, mysql_tables, new_tables)
                # self.add_missing_columns(mysql_cursor, snfk_cursor, source_bucket, mysql_tables, table_changes,
                #                          destination_bucket)
                self.sync_columns(mysql_cursor, snfk_cursor, mysql_tables, table_changes,
                                  source_bucket, destination_bucket)

        logging.info("All tables synced.")

    def run_fetch_tables(self):

        sapi_token = self.parameters.sapi_token
        self.create_api_clients(sapi_token)
        configs_parsed, max_index = self.get_and_parse_configurations(self.sapi)

        already_synced_tables = []

        for _, items in configs_parsed.items():
            already_synced_tables += items['tables']

        self._mysql_create_cursor()

        with self.mysql_conn:
            with self.mysql_conn.cursor() as cursor:
                mysql_tables = self.mysql_fetch_all_tables(cursor)

        mysql_tables_names = []
        for _, tables in mysql_tables.items():
            mysql_tables_names += [t.full_name for t in tables]

        self.to_sync = []

        for table in mysql_tables[self.parameters.schemas[0]]:
            if table.full_name not in self.parameters.exclude_tables and table.full_name not in already_synced_tables:
                self.to_sync += [table]

        self.to_sync.sort(key=operator.attrgetter('name'))
        self.state_map = {}
        for table in self.to_sync:
            self.state_map[table.full_name] = table.state_name

        if len(self.to_sync) == 0:
            logging.info("No new tables found to sync.")
            exit(0)

        self.to_sync_sep = []

        if self.parameters.side_load.use and (self.parameters.side_load.rows or self.parameters.side_load.size):
            logging.debug("Evaluating tables for side loading with parameters: "
                          f"{dataclasses.asdict(self.parameters.side_load)}")

            if self.parameters.orchestration_add:

                for table in self.to_sync:
                    if self.is_table_separate_sync(table, self.parameters.side_load):
                        self.to_sync_sep += [table]

                for table in self.to_sync_sep:
                    self.to_sync.remove(table)

        logging.info(f"{len(self.to_sync)} tables will be added to configurations directly.")

        free_spaces = self.get_free_configurations(configs_parsed, self.parameters.nr_tables_in_config)
        self.update_configurations_with_new_tables(self.sapi, configs_parsed, free_spaces)

        if len(self.to_sync) != 0:
            logging.info(f"No free configurations left with less than {self.parameters.nr_tables_in_config} tables. "
                         "Creating new configurations.")

            self.create_configurations_with_new_tables(self.sapi, self.encr, self.orch, max_index)

        if len(self.to_sync_sep) != 0:
            logging.info("Following tables will be backfilled separately: "
                         f"{', '.join([t.full_name for t in self.to_sync_sep])}")
            new_config_id = self.create_temporary_backfill_configuration(self.sapi, self.encr)

            try:
                self.run_temporary_backfill_configuration(new_config_id)
                new_state = self.get_state_of_backfill_configuration(new_config_id)

                configs_parsed, max_index = self.get_and_parse_configurations(self.sapi)
                free_spaces = self.get_free_configurations(configs_parsed, self.parameters.nr_tables_in_config)

                self.to_sync = self.to_sync_sep
                self.disable_orchestration_and_wait_for_job(self.parameters.orchestration_add_id)
                self.update_configurations_with_new_tables(self.sapi, configs_parsed, free_spaces, new_state)

                if len(self.to_sync) != 0:
                    logging.info(f"No free configurations left with less than {self.parameters.nr_tables_in_config} "
                                 "tables. Creating new configurations.")

                    self.create_configurations_with_new_tables(self.sapi, self.encr, self.orch, max_index, new_state)
            finally:
                self.sapi.delete_configuration(COMPONENT_ID, new_config_id)
                self.enable_orchestration(self.orch, self.parameters.orchestration_add_id)

        logging.info("Fetching of new tables finished.")

    def get_state_of_backfill_configuration(self, configuration_id: int):

        cfg_rsp = self.sapi.get_configuration_detail(COMPONENT_ID, configuration_id)
        state = {}

        if cfg_rsp.ok:
            bookmarks = cfg_rsp.json()['state']['component']['bookmarks']

            for table, table_state in bookmarks.items():
                state[table] = table_state

            return state

        else:
            raise Exception("Could not obtain state of temporary configuration")

    def run_temporary_backfill_configuration(self, configuration_id: int):

        job_rsp = self.docker.create_job(COMPONENT_ID, configuration_id)

        if job_rsp.ok:
            job_id = job_rsp.json()['id']
            job_url = job_rsp.json()['url']
            logging.info(f"Created job {job_id} for temporary backfill configuration {configuration_id}.")
        else:
            logging.error(f"Could not create job for temporary configuration.\n{job_rsp.json()}")
            exit(1)

        job_status, job_json = self.docker.pool_job(job_url, 15)

        if job_status != 'success':
            raise Exception(f"Temporary configuration job #1 ended with error.\n{job_json}")

        job2_rsp = self.docker.create_job(COMPONENT_ID, configuration_id)

        if job2_rsp.ok:
            job2_id = job2_rsp.json()['id']
            job2_url = job2_rsp.json()['url']
            logging.info(f"Created job {job2_id} for temporary backfill configuration {configuration_id}.")
        else:
            logging.error(f"Could not create job for temporary configuration.\n{job2_rsp.json()}")
            exit(1)

        job2_status, job2_json = self.docker.pool_job(job2_url, 15)

        if job2_status != 'success':
            raise Exception(f"Temporary configuration job ended with error.\n{job2_json}")

    def create_temporary_backfill_configuration(self, sapi_client: StorageApi, encrypt_client: EncryptionApi):

        tables_json, tables = self.create_json_for_temporary_backfill_configuration()

        configuration = copy.deepcopy(self.parameters.cdc_config)
        configuration['inputMappingsJson'] = json.dumps(tables_json, indent=2)
        configuration['host'] = self.mysql.host
        configuration['port'] = self.mysql.port
        configuration['username'] = self.mysql.username
        configuration['#password'] = self.mysql.password

        configuration_encrypted = encrypt_client.encrypt_data(configuration)
        config_name = self.create_configuration_name(0, tables['tables'])

        config = {'name': config_name, 'configuration': json.dumps({'parameters': configuration_encrypted})}
        new_config_rsp = sapi_client.create_new_configuration(COMPONENT_ID, config)

        if new_config_rsp.status_code == 201:
            logging.info(f"Created new configuration for tables {', '.join(tables['tables'])}.")
            new_config_id = new_config_rsp.json()['id']

        else:
            logging.error("Could not create new configuration."
                          f"Received: {new_config_rsp.status_code} - {new_config_rsp.json()}.")
            exit(1)

        return new_config_id

    def create_json_for_temporary_backfill_configuration(self):

        tables_in_config = []
        tables_json = {}

        for table in self.to_sync_sep:
            if table.schema not in tables_json:
                tables_json[table.schema] = {'tables': []}

            tables_json[table.schema]['tables'] += [{table.name: {
                'selected': True,
                'replication-method': 'log_based'
            }}]

            tables_in_config += [table.full_name]

        return tables_json, {'tables': tables_in_config, 'new': tables_in_config}

    def is_table_separate_sync(self, table: MySQLTable, side_load_parameters: SideLoadParameters):

        rows_limit = table.rows if not side_load_parameters.rows else side_load_parameters.rows
        size_limit = table.size_mb if not side_load_parameters.size else side_load_parameters.size

        if table.rows >= rows_limit and table.size_mb >= size_limit:
            return True
        else:
            False

    def run_fetch_tables_old(self):

        sapi_token = self.parameters.sapi_token
        self.create_api_clients(sapi_token)
        configs_parsed, max_index = self.get_and_parse_configurations(self.sapi)

        already_synced_tables = []

        for _, items in configs_parsed.items():
            already_synced_tables += items['tables']

        self._mysql_create_cursor()

        with self.mysql_conn:
            with self.mysql_conn.cursor() as cursor:
                mysql_tables_raw = self.mysql_fetch_all_tables(cursor)

        mysql_tables = []
        for _, tables in mysql_tables_raw.items():
            mysql_tables += [t.full_name for t in tables]

        self.to_sync = list(set(mysql_tables) - set(self.parameters.exclude_tables) - set(already_synced_tables))
        self.to_sync.sort()

        if len(self.to_sync) == 0:
            logging.info("No new tables found to sync.")
            exit(0)

        free_spaces = self.get_free_configurations(configs_parsed, self.parameters.nr_tables_in_config)
        self.update_configurations_with_new_tables(self.sapi, configs_parsed, free_spaces)

        if len(self.to_sync) != 0:
            logging.info(f"No free configurations left with less than {self.parameters.nr_tables_in_config} tables. "
                         "Creating new configurations.")

            self.create_configurations_with_new_tables(self.sapi, self.encr, self.orch, max_index)

        logging.info("Fetching of new tables finished.")

    def run_remove_obsolete_tables(self):

        self.create_api_clients(self.parameters.sapi_token)
        self._mysql_create_cursor()

        with self.mysql_conn:
            with self.mysql_conn.cursor() as mysql_cursor:
                mysql_tables = self.mysql_fetch_all_tables(mysql_cursor)

        configs, _ = self.get_and_parse_configurations(self.sapi)
        schema_to_watch = self.parameters.schemas[0]

        current_tables = []
        for _, cfg in configs.items():
            for t in cfg['tables']:
                if t.startswith(f'{schema_to_watch}.'):
                    current_tables += [t]

        try:
            mysql_available_tables = [t.full_name for t in mysql_tables[schema_to_watch]]
        except KeyError:
            logging.error(f"Schema {schema_to_watch} is not available in MySQL.")
            raise

        inactive_tables = set(current_tables) - set(mysql_available_tables)

        for cfg_id, cfg in configs.items():

            cfg_name = cfg['name']
            tables_in_cfg = set(cfg['tables'])
            remove_tables = inactive_tables.intersection(tables_in_cfg)
            active_tables = tables_in_cfg - remove_tables

            if len(remove_tables) > 0:
                logging.info(f"Removing inactive tables from configuration {cfg_id} - {cfg_name}: {remove_tables}.")

                old_table_cfg = json.loads(cfg['tables_json_str'])
                new_table_cfg = dict()

                for schema, tables in old_table_cfg.items():

                    if schema == schema_to_watch:
                        new_table_cfg[schema_to_watch] = {'tables': []}

                        for table in tables['tables']:
                            if f'{schema_to_watch}.{list(table.keys())[0]}' in remove_tables:
                                continue
                            else:
                                new_table_cfg[schema_to_watch]['tables'] += [table]
                    else:
                        new_table_cfg[schema] = tables

                cfg_parameters = copy.deepcopy(cfg['api']['configuration'])
                cfg_parameters['parameters']['inputMappingsJson'] = json.dumps(new_table_cfg, indent=2)

                active_tables_list = list(active_tables)
                active_tables_list.sort()

                new_name = f"{cfg['index']:02} - {', '.join(active_tables_list)}"

                cfg_json = {
                    'configuration': json.dumps(cfg_parameters),
                    'name': new_name,
                    'changeDescription': f'Removed inactive tables from configuration: {remove_tables}.'
                }
                update_rsp = self.sapi.update_configuration(COMPONENT_ID, cfg_id, cfg_json)

                if update_rsp.ok:
                    logging.info(f"Removed tables {remove_tables} from configuration {cfg_id} - {new_name}.")

    def run(self):

        if self.mode == 'fetch_new_tables':
            self.run_fetch_tables()

        if self.mode == 'sync_tables':
            self.run_sync_tables()

        if self.mode == 'historize_tables':
            self.run_historize_tables()

        if self.mode == 'remove_obsolete_tables':
            self.run_remove_obsolete_tables()
